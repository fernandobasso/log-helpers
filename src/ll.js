/* eslint-disable no-console */

const l = require('./l');

module.exports = label => {
  return obj => {
    l(label, obj);
    return obj;
  };
};
