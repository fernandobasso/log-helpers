module.exports = {
  l: require('./l'),
  i: require('./i'),
  w: require('./w'),
  e: require('./e'),
  ll: require('./ll'),
};
