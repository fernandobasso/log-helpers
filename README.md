# Log Helpers 

A few log helpers utilities. These are the simpler helpers:

- `l`, short for `console.log`
- `i`, short for `console.info`
- `w`, short for `console.warn`
- `e`, short for `console.error`

And a logger function to help when inside `compose` or `pipe`-like functions that required curried functions.

- `ll`


# Examples

The simpler `l`, `i`, `w` and `e` helpers are used simply like this:

```js
const mystr = 'may the force';
l('value of the string', mystr);

# or
w('invalid input, using default value');
```

## The Curried `ll`

The `ll` function can be used inside function composition situations that require curried functions. In such cases, the simpler `console.log` function and its variants don't work and therefore we need a curried version of the logger functions.

Suppose you are using Ramda's `pipe` function:

```js
pipe(
  prop('langs'),
  ll('value of langs'), // <1>
  toUpper,
);
```

1. You use the `ll` helper passing a label parameter to help identify the log on the console, and `pipe` takes care of passing the value to the `ll` function.

